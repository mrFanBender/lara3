<?php

namespace App\Http\Controllers\Tasks;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Task;
use App\Http\Requests\createTasksRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;

class TasksController extends Controller
{

    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', ['tasks'=>$tasks]);
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function store(createTasksRequest $request)
    {
        $task = new Task();
        $task->fill($request->all());

        $task->save();

        return redirect()->route('tasks.index');
    }

    public function edit($id)
    {
        $task = Task::find($id);

        return view('tasks.edit', ['task' => $task]);
    }

    public function update(createTasksRequest $request, $id)
    {
        $task = Task::find($id);
        $task->fill($request->all());
        $task->save();

        return redirect()->route('tasks.index');
    }

    public function destroy ($id)
    {
        $task = Task::find($id);
        $task->delete();

        return redirect()->route('tasks.index');
    }
}
