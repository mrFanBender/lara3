@extends('layout')

@section('content')
    <div class="container">
        <h3>Edit Task # {{$task->id}}</h3>
        @include('tasks.errors')
        <div class="row">
           <div class="col-md-10 col-md-offset-1">
               {!! Form::open(['route' => ['tasks.update', $task->id], 'method'=>'PUT']) !!}
               <div class="form-group">
                   <input type="text" class="form-control" name="title" value="{{$task->title}}"/>
                   <br/>
                   <textarea class="form-control" name="description" cols="30" rows="10">{{$task->description}}</textarea>
                   <br/>
                   <button class="btn btn-success">Update</button>
               </div>
               {!! Form::close() !!}
           </div>
        </div>
    </div>

@endsection
