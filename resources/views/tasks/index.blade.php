@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <h3>My Tasks</h3>
            <div class="col-md-12">
                <a href="{{route('tasks.create')}}" class="btn btn-success">New task</a>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <table class="table">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Title</td>
                        <td>Action</td>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                        <tr>
                            <td>{{$task->id}}</td>
                            <td>{{$task->title}}</td>
                            <td>
                                <a href="{{route('tasks.edit', ['id'=>$task->id])}}">
                                    <i class="glyphicon glyphicon-eye-open"></i>
                                </a>
                                {{Form::open(['method'=>'DELETE', 'route'=>['tasks.destroy',$task->id]])}}
                                    <button class="btn-delete" onclick="return confirm('Вы уверены, что хотите удалить запись?')">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </button>
                                {{Form::close()}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
