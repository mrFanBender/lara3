@extends('layout')

@section('content')
    <div class="container">
        <h3>Create new task</h3>
        @include('tasks.errors')
        <div class="row">
           <div class="col-md-10 col-md-offset-1">
               {!! Form::open(['route' => ['tasks.store']]) !!}
               <div class="form-group">
                   <input type="text" class="form-control" name="title" value="{{old('title')}}"/>
                   <br/>
                   <textarea class="form-control" name="description" cols="30" rows="10">{{old('description')}}</textarea>
                   <br/>
                   <button class="btn btn-success">Create</button>
               </div>
               {!! Form::close() !!}
           </div>
        </div>
    </div>

@endsection
